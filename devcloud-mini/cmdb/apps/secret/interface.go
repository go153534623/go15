package secret

import (
	"context"

	"github.com/infraboard/mcube/v2/http/request"
	"github.com/infraboard/mcube/v2/ioc"
	"github.com/infraboard/mcube/v2/tools/pretty"
	"github.com/infraboard/mcube/v2/types"
)

const (
	AppName = "secret"
)

func GetService() Service {
	return ioc.Controller().Get(AppName).(Service)
}

type Service interface {
	//
	CreateSecret(context.Context, *CreateSecretRequest) (*Secret, error)
	//
	QuerySecret(context.Context, *QuerySecretRequest) (*types.Set[*Secret], error)
	//
	DescribeSecret(context.Context, *DescribeSecretRequeset) (*Secret, error)

	// 怎么API怎么设计
	// 同步阿里云所有资源, 10分钟，30分钟 ...
	// 这个接口调用持续30分钟...
	// Req ---> <---- Resp:   能快速响应的同步调用

	// Stream API
	SyncResource(context.Context, *SyncResourceRequest, SyncResourceHandleFunc) error
}

type SyncResourceHandleFunc func(ResourceResponse)

type ResourceResponse struct {
	Success    bool
	InstanceId string `json:"instance_id"`
	Message    string `json:"message"`
}

func (t ResourceResponse) String() string {
	return pretty.ToJSON(t)
}

func NewQuerySecretRequest() *QuerySecretRequest {
	return &QuerySecretRequest{
		PageRequest: request.NewDefaultPageRequest(),
	}
}

type QuerySecretRequest struct {
	// 分页请求
	*request.PageRequest
}

func NewDescribeSecretRequeset(id string) *DescribeSecretRequeset {
	return &DescribeSecretRequeset{
		Id: id,
	}
}

type DescribeSecretRequeset struct {
	Id string `json:"id"`
}

func NewSyncResourceRequest() *SyncResourceRequest {
	return &SyncResourceRequest{}
}

type SyncResourceRequest struct {
	Id string `json:"id"`
}
