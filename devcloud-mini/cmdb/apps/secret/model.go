package secret

import (
	"encoding/base64"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/infraboard/mcube/v2/crypto/cbc"
	"github.com/infraboard/mcube/v2/ioc/config/application"
	"github.com/infraboard/mcube/v2/tools/pretty"
	"github.com/infraboard/mcube/v2/types"
	"gitlab.com/go-course-project/go15/devcloud-mini/cmdb/apps/resource"
)

func NewSecretSet() *types.Set[*Secret] {
	return types.New[*Secret]()
}

func NewSecret(in *CreateSecretRequest) *Secret {
	//  hash版本的UUID
	// 	Vendor Address ApiKey
	uid := uuid.NewMD5(uuid.Nil, []byte(fmt.Sprintf("%d.%s.%s", in.Vendor, in.Address, in.ApiKey))).String()
	return &Secret{
		Id:                  uid,
		UpdateAt:            time.Now().Unix(),
		CreateSecretRequest: in,
	}
}

type Secret struct {
	Id                   string `json:"id" bson:"_id"`
	UpdateAt             int64  `json:"update_at" bson:"update_at"`
	*CreateSecretRequest `bson:"inline"`
}

func (s *Secret) String() string {
	return pretty.ToJSON(s)
}

func NewCreateSecretRequest() *CreateSecretRequest {
	return &CreateSecretRequest{
		Regions: []string{},
	}
}

type CreateSecretRequest struct {
	//
	Vendor resource.VENDOR `json:"vendor"`
	// Vmware
	Address string `json:"address"`
	//
	ApiKey string `json:"api_key"`
	//
	ApiSecret string `json:"api_secret"`
	//
	isEncrypted bool

	// 资源所在区域
	Regions []string `json:"regions"`
}

func (r *CreateSecretRequest) SetisEncrypted(v bool) {
	r.isEncrypted = v
}

func (r *CreateSecretRequest) EncryptedApiSecret() error {
	if r.isEncrypted {
		return nil
	}
	// Hash, 对称，非对称
	// 对称加密 AES(cbc)
	// @v1,xxxx@xxxxx
	cipherText, err := cbc.Encrypt([]byte(r.ApiSecret), []byte(application.Get().EncryptKey))
	if err != nil {
		return err
	}
	r.ApiSecret = base64.StdEncoding.EncodeToString(cipherText)
	r.SetisEncrypted(true)
	return nil

}

func (r *CreateSecretRequest) DecryptedApiSecret() error {
	if r.isEncrypted {
		cipherdText, err := base64.StdEncoding.DecodeString(r.ApiSecret)
		if err != nil {
			return err
		}

		plainText, err := cbc.Decrypt(cipherdText, []byte(application.Get().EncryptKey))
		if err != nil {
			return err
		}
		r.ApiSecret = string(plainText)
		r.SetisEncrypted(false)
	}
	return nil
}
