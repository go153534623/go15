package impl_test

import (
	"os"
	"testing"

	"gitlab.com/go-course-project/go15/devcloud-mini/cmdb/apps/secret"
)

func TestCreateSecret(t *testing.T) {
	req := secret.NewCreateSecretRequest()
	req.ApiKey = "AKIDNhw1udKxWuAIJBvTrCNPG3lKtLj5Mbhf"
	req.ApiSecret = os.Getenv("TX_SECRET_KEY")
	req.Regions = []string{"ap-guangzhou"}
	s, err := svc.CreateSecret(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(s)
}

func TestQuerySecret(t *testing.T) {
	req := secret.NewQuerySecretRequest()
	set, err := svc.QuerySecret(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}

func TestDescribeSecret(t *testing.T) {
	req := secret.NewDescribeSecretRequeset("05fdac51-cb80-32ee-996a-ef7a781dd392")
	set, err := svc.DescribeSecret(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}

func TestSyncResource(t *testing.T) {
	req := secret.NewSyncResourceRequest()
	req.Id = "05fdac51-cb80-32ee-996a-ef7a781dd392"
	// 主机决定怎么处理
	err := svc.SyncResource(ctx, req, func(r secret.ResourceResponse) {
		//	通过Websocket 返回给 Web
		t.Log(r)
	})
	if err != nil {
		t.Fatal(err)
	}
}
