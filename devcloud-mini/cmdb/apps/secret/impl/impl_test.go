package impl_test

import (
	"context"

	"github.com/infraboard/mcube/v2/ioc"

	_ "gitlab.com/go-course-project/go15/devcloud-mini/cmdb/apps"
	"gitlab.com/go-course-project/go15/devcloud-mini/cmdb/apps/secret"
)

var (
	ctx = context.Background()
	svc = secret.GetService()
)

func init() {
	ioc.DevelopmentSetup()
}
