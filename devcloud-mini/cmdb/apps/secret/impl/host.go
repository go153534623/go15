package impl

import (
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	lighthouse "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/lighthouse/v20200324"
	"gitlab.com/go-course-project/go15/devcloud-mini/cmdb/apps/resource"
)

// 转化逻辑
// https://cloud.tencent.com/document/api/1207/47576#Instance
func (i *impl) TransfterLighthouseInstance(ins *lighthouse.Instance) *resource.Resource {
	res := resource.NewResource()

	res.Id = GetValue(ins.InstanceId)
	res.Name = GetValue(ins.InstanceName)
	res.Cpu = GetValue(ins.CPU)
	res.Memory = GetValue(ins.Memory)
	res.Storage = GetValue(ins.SystemDisk.DiskSize)
	res.PrivateAddress = common.StringValues(ins.PrivateAddresses)
	return res
}
