package impl

func GetValue[T any](s *T) T {
	if s == nil {
		var zero T
		return zero
	}

	return *s
}
