package api

import (
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/v2/http/request"
	"github.com/infraboard/mcube/v2/http/restful/response"
	"gitlab.com/go-course-project/go15/devcloud-mini/cmdb/apps/secret"
)

func (h *handler) DescribeSecret(r *restful.Request, w *restful.Response) {
	req := secret.NewDescribeSecretRequeset(r.PathParameter("id"))
	ins, err := h.svc.DescribeSecret(r.Request.Context(), req)
	if err != nil {
		response.Failed(w, err)
		return
	}

	response.Success(w, ins)
}

// 需要脱敏
func (h *handler) QuerySecret(r *restful.Request, w *restful.Response) {
	req := secret.NewQuerySecretRequest()
	req.PageRequest = request.NewPageRequestFromHTTP(r.Request)
	set, err := h.svc.QuerySecret(r.Request.Context(), req)
	if err != nil {
		response.Failed(w, err)
		return
	}
	response.Success(w, set)
}

// web socket
func (h *handler) SyncResource(r *restful.Request, w *restful.Response) {
	req := secret.NewSyncResourceRequest()
	req.Id = r.PathParameter("id")

	// websocket, 返回给前端
	// ws conn
	err := h.svc.SyncResource(r.Request.Context(), req, func(rr secret.ResourceResponse) {
		h.log.Debug().Msgf("%s", rr)
		// conn.write
	})
	if err != nil {
		response.Failed(w, err)
		return
	}
}
