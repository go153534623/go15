package common

// 构造函数
func NewResourceSet[T any]() *ResourceSet[T] {
	return &ResourceSet[T]{
		Items: []T{},
	}
}

// 定义一个泛型类型 T
//
//	创建一个 ResourceSet，处理 Resource 类型
//
// resourceSet := ResourceSet[*Resource]{}
type ResourceSet[T any] struct {
	Total int64 `json:"total"`
	Items []T   `json:"items"`
}

// Add 方法用于添加资源
func (s *ResourceSet[T]) Add(items ...T) {
	s.Items = append(s.Items, items...)
	s.Total += int64(len(items)) // 更新总数
}
