package api

import (
	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/infraboard/mcube/v2/exception"
	"github.com/infraboard/mcube/v2/ioc"
	"github.com/infraboard/mcube/v2/ioc/config/gorestful"
	"github.com/infraboard/mcube/v2/ioc/config/log"
	"github.com/rs/zerolog"
	"gitlab.com/go-course-project/go15/devcloud-mini/maudit/apps/event"
)

func init() {
	ioc.Api().Registry(&handler{})
}

type handler struct {
	ioc.ObjectImpl

	// 模块子Logger
	log *zerolog.Logger
	// svc controller
	svc event.Service
}

func (h *handler) Name() string {
	return event.AppName
}

func (h *handler) Init() error {
	tags := []string{"审计日志"}

	// 对象
	h.log = log.Sub(h.Name())
	h.svc = event.GetService()

	// 设置路由, 获取RootRouter
	// 在ioc里面获取 go restful的 Container
	// 怎么查看有哪些可用路有, 直接集成swagger模块
	// https://github.com/infraboard/mcube/blob/be646a3d8bc93fccec7de1be785f895d24752406/ioc/apps/health/restful/check.go#L4
	ws := gorestful.ObjectRouter(h)
	ws.Route(ws.GET("").To(h.QueryEvent).Doc("审计日志列表查询").
		Param(ws.PathParameter("page_size", "页大小").DataType("int").Required(false).DefaultValue("20")).
		Param(ws.PathParameter("page_number", "页码").DataType("int").Required(false).DefaultValue("1")).
		// restfulspec  是 gorestful框架提供的 集成 "github.com/emicklei/go-restful-openapi/v2"
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Writes(event.NewEventSet()).
		Returns(200, "OK", event.NewEventSet()).
		Returns(404, "Not Found", exception.ApiException{}))
	return nil
}
