package apps

import (
	_ "gitlab.com/go-course-project/go15/devcloud-mini/maudit/apps/event/api"
	_ "gitlab.com/go-course-project/go15/devcloud-mini/maudit/apps/event/consumer"
	_ "gitlab.com/go-course-project/go15/devcloud-mini/maudit/apps/event/impl"
)
