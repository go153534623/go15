# 如何使用中间件接入

```go
// 接入审计中间件
import (
    _ "gitlab.com/go-course-project/go15/devcloud-mini/maudit/client/middleware"
)
```

配置
```toml
[kafka]
  brokers = ["127.0.0.1:9092"]
  debug = false

[maudit_client]
maudit_topic_name = "maudit"
```

路有装饰
```go
	// 服务(启动的时候获取本服务的名称), 资源类型, 动作
	ws.Route(ws.GET("").To(h.QuerySecret).
		Metadata(label.Audit, label.Enable).
		Metadata(label.Resource, h.Name()).
		Metadata(label.Action, label.List.Value()))
```

## 测试

```sh
$ curl localhost:8020/api/cmdb/v1/secret 

$ curl localhost:8010/api/maudit/v1/event
{
 "total": 0,
 "items": [
  {
   "id": "cr4qk91uevgqf1risbig",
   "who": "",
   "time": 1724492324,
   "ip": "",
   "user_agent": "curl/8.6.0",
   "service": "cmdb",
   "resource_type": "secret",
   "action": "list",
   "resource_id": "",
   "status_code": 200,
   "error_message": "",
   "label": {},
   "extras": {
    "method": "GET",
    "operation": "QuerySecret",
    "path": "/api/cmdb/v1/secret/"
   }
  }
 ]
}
```