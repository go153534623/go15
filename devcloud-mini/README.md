# 简化版本的研发云

## 如何搭建微服务开发环境

多个项目，使用一个workspace组合起来

```sh
go work init

## 初始化3个项目(到各种的目录)
go mod init "gitlab.com/go-course-project/go15/devcloud-mini/mcenter" 
go mod init "gitlab.com/go-course-project/go15/devcloud-mini/maudit" 
go mod init "gitlab.com/go-course-project/go15/devcloud-mini/cmdb" 

## 讲3个项目 加载到一个 workspace中
go work use mcenter
go work use maudit
go work use cmdb

## 将整个微服务工程当作一个项目来打开
cd devcloud-mini
code .
```

## workspace 的优势




