# Protobuf

vscode语法插件: vscode-proto3

```proto
syntax = "proto3";

package hello;
option go_package="gitlab.com/go-course-project/go15/skills/protobuf";

message String {
    string value = 1;
}
```

```go
type String struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Value string `protobuf:"bytes,1,opt,name=value,proto3" json:"value,omitempty"`
}
```

```sh
$ cd go15 ## 以day21作为编译的工作目录
$ protoc -I=. --go_out=.  --go_opt=module="gitlab.com/go-course-project/go15" skills/protobuf/hello.proto
$ protoc-go-inject-tag -input="skills/protobuf/*.pb.go"

// 依赖包也需要编译
// 使用通配符指定所有需要编译的protobuf文件
protoc -I=. --go_out=.  --go_opt=module="gitlab.com/go-course-project/go15" skills/protobuf/*/*.proto
protoc -I=. --go_out=.  --go_opt=module="gitlab.com/go-course-project/go15" skills/protobuf/*.proto
```
