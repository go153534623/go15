package service

// Hello: request ---> response
// Server 实现和 Client的实现
// 必须要满足 rpc 框架的接口声明 fn(request, response) error
type Service interface {
	Hello(string, *string) error
}

type HelloRequest struct {
	Name string
}

type HelloResponse struct {
	Message string
}
