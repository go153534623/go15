# 浏览器

[浏览器](https://gitee.com/infraboard/go-course/blob/master/day19/browser.md)



## Ajax

```sh
fetch('http://127.0.0.1:8080/vblog/api/v1/blogs/')
  .then(response => response.json())
  .then(data => console.log(data))
  .catch(error => console.error('请求失败', error));
```

服务端 怎么添加 CROS

## 存储方案

[存储方案](https://gitee.com/infraboard/go-course/blob/master/day20/vue-store.md#%E6%B5%8F%E8%A7%88%E5%99%A8%E6%9C%AC%E5%9C%B0%E5%AD%98%E5%82%A8)

+ localStorage: 网站级别
+ sessionStorage: 窗口级别