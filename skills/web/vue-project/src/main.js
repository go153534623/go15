import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

const app = createApp(App)

app.use(createPinia())
app.use(router)


// component 往当前app注册全局组件
import TestComponent from './components/TestComponent.vue'
app.component('TestComponent', TestComponent)

app.mount('#app')
