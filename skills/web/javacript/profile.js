// profile.js
export var firstName = "Michael";
export var lastName = "Jackson";
export var year = 1958;

// 唯一的全局变量MYAPP:
// export var MYAPP = {
//   name: "myapp",
//   version: 1.0,
//   foo: function () {
//     return "foo";
//   },
// };



// // 默认导出 DEFAULT
// export default {
//   name: "myapp",
//   version: 1.0,
//   foo: function () {
//     return "foo";
//   },
// };


// 通过回调处理数据
// 定义一个函数, 这个函数是网络Io block, Js 调用立即返回
// 没办法拿到网络请求成功或者失败后的 返回结果
// 必须定义回调:
//   1. 请求成功回调: success
//   2. 请求失败回调: failed
function testResultCallbackFunc(success, failed) {
    // timeOut 是一个随机数  0 ～ 2
    var timeOut = Math.random() * 2;
    console.log('set timeout to: ' + timeOut + ' seconds.');

    // 一秒后触发一个函数
    // 模拟1秒后返回结果, 
    // < 1 成功 success
    // > 1 超时失败 failed
    setTimeout(function () {
        if (timeOut < 1) {
            console.log('call success()...');
            success('200 OK');
        }
        else {
            console.log('call failed()...');
            failed('timeout in ' + timeOut + ' seconds.');
        }
    }, timeOut * 1000);
}

// //  直接使用
// testResultCallbackFunc(
//     (data) => {
//         console.log(data)
//     },
//     (err) => {
//         console.log(err)
//     }
// )

// // 你不能指望前面同步执行完成
// console.log("testResultCallbackFunc call 完成")


// // 采用Promise对象封装, 封装成Promise, 发明一个Promise语法
// // 成功调用 then(fn)
// // 失败调用 catch(fn)
// // 写法上更优雅, 同时也统一了语法
// var p1 = new Promise(testResultCallbackFunc)
// p1.then((resp) => {
//     console.log(resp)
//     // p2.then((v) => {
//     //     p3.then((v) => {

//     //     })
//     // }).cache((e) => {
//     //     p4.then((v)=> {
            
//     //     })
//     // })
// }).catch((err) => {
//     console.log(err)
// })


// 生命这个函数 在excutor中允许
// 由excturo的 await 函数获取 异步对象的返回结果
// 从此告别回调地狱, 本质还是回调执行, 有Excutor 等待并通知结果
// async + await  使用同步的写法来写异步程序
async function testWithAsync() {
    var p1 = new Promise(testResultCallbackFunc)
    var resp1 = await p1
    var p2 = new Promise(testResultCallbackFunc)
    var resp2 = await p2
    var p3 = new Promise(testResultCallbackFunc)
    var resp3 = await p3
}

testWithAsync()