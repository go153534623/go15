# MongoDB CRUD怎么写

[mongo组件](https://www.mcube.top/docs/framework/component/mongo/)


## 准备一个MongoDB数据库

```
docker run -itd -p 27017:27017 --name mongo mongo
```

使用客户端工具来连接:  Navicat, VsCode插件: MySQL

## 如何使用官方SDK 连接MongoDB: Client

使用官方SDK库: "go.mongodb.org/mongo-driver/mongo"

```go
type mongoDB struct {
	Endpoints   []string `toml:"endpoints" json:"endpoints" yaml:"endpoints" env:"ENDPOINTS" envSeparator:","`
	UserName    string   `toml:"username" json:"username" yaml:"username"  env:"USERNAME"`
	Password    string   `toml:"password" json:"password" yaml:"password"  env:"PASSWORD"`
	Database    string   `toml:"database" json:"database" yaml:"database"  env:"DATABASE"`
	AuthDB      string   `toml:"auth_db" json:"auth_db" yaml:"auth_db"  env:"AUTH_DB"`
	EnableTrace bool     `toml:"enable_trace" json:"enable_trace" yaml:"enable_trace"  env:"ENABLE_TRACE"`

	client *mongo.Client
}

func (m *mongoDB) GetDB() *mongo.Database {
	return m.client.Database(m.Database)
}

// Client 获取一个全局的mongodb客户端连接
func (m *mongoDB) Client() *mongo.Client {
	return m.client
}

func (m *mongoDB) getClient() (*mongo.Client, error) {
	opts := options.Client()

	if m.UserName != "" && m.Password != "" {
		cred := options.Credential{
			AuthSource: m.GetAuthDB(),
		}

		cred.Username = m.UserName
		cred.Password = m.Password
		cred.PasswordSet = true
		opts.SetAuth(cred)
	}
	opts.SetHosts(m.Endpoints)
	opts.SetConnectTimeout(5 * time.Second)

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*5))
	defer cancel()

	// Connect to MongoDB
	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		return nil, fmt.Errorf("new mongodb client error, %s", err)
	}

	if err = client.Ping(ctx, nil); err != nil {
		return nil, fmt.Errorf("ping mongodb server(%s) error, %s", m.Endpoints, err)
	}

	return client, nil
}
```

## 编写 MongoDB CRUD

[官方CRUD](https://www.mongodb.com/docs/manual/crud/)

[官方样例](https://github.com/mongodb/mongo-go-driver/blob/v1/examples/documentation_examples/examples.go)

[InsertExamples](https://github.com/mongodb/mongo-go-driver/blob/v1/examples/documentation_examples/examples.go#L55)
```go
result, err := coll.InsertOne(
    context.TODO(),
    bson.D{
        {"item", "canvas"},
        {"qty", 100},
        {"tags", bson.A{"cotton"}},
        {"size", bson.D{
            {"h", 28},
            {"w", 35.5},
            {"uom", "cm"},
        }},
    })
```

[QueryToplevelFieldsExamples](https://github.com/mongodb/mongo-go-driver/blob/v1/examples/documentation_examples/examples.go#L144)
```go
cursor, err := coll.Find(
    context.TODO(),
    bson.D{{"item", "canvas"}},
)
```

[UpdateExamples](https://github.com/mongodb/mongo-go-driver/blob/v1/examples/documentation_examples/examples.go#L1356)
```go
result, err := coll.UpdateOne(
    context.TODO(),
    bson.D{
        {"item", "paper"},
    },
    bson.D{
        {"$set", bson.D{
            {"size.uom", "cm"},
            {"status", "P"},
        }},
        {"$currentDate", bson.D{
            {"lastModified", true},
        }},
    },
)
```

[DeleteExamples](https://github.com/mongodb/mongo-go-driver/blob/v1/examples/documentation_examples/examples.go#L1637)
```go
result, err := coll.DeleteMany(
    context.TODO(),
    bson.D{
        {"status", "A"},
    },
)
```
