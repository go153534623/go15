# GoRestful 框架使用

[官方文档](https://github.com/emicklei/go-restful)

```go
// https://github.com/emicklei/go-restful/blob/master/examples/restful-multi-containers.go
// GET http://localhost:8080/hello
// GET http://localhost:8081/hello
package main

import (
    "github.com/emicklei/go-restful/v3"
    "io"
    "log"
    "net/http"
)

func main() {
  // add to default container
    ws := new(restful.WebService)
    ws.Route(ws.GET("/hello").To(hello))
    restful.Add(ws)
    go func() {
        log.Fatal(http.ListenAndServe(":8080", nil))
    }()

  // container 2
    container2 := restful.NewContainer()
    ws2 := new(restful.WebService)
    ws2.Route(ws2.GET("/hello").To(hello2))
    container2.Add(ws2)

    server := &http.Server{Addr: ":8081", Handler: container2}
    log.Fatal(server.ListenAndServe())
}

func hello(req *restful.Request, resp *restful.Response) {
    io.WriteString(resp, "default world")
}

func hello2(req *restful.Request, resp *restful.Response) {
    io.WriteString(resp, "second world")
}
```

+ WebService: gin.Group(), ws.GET("/hello").To(hello) ==>  r.GET("/xxx", hello)
+ Container: 装载webservice的容器, gin 的Engine对象, 多个group的容器 group(group())

![alt text](image.png)

参考[k8s APIServer源码: go-restful框架](https://wklken.me/posts/2017/09/23/source-apiserver-01.html)


## 支持路有装饰

```go
ws.Route(ws.GET("/{id}").To(h.DescribeBuildConfig).
  Doc("构建配置详情").
  Param(ws.PathParameter("id", "identifier of the secret").DataType("string")).
  Metadata(restfulspec.KeyOpenAPITags, tags).
  Writes(build.BuildConfig{}).
  Returns(200, "OK", build.BuildConfig{}).
  Returns(404, "Not Found", nil))
```

```
"/{id}" --> h.DescribeBuildConfig
```
+ swagger 文档装饰
+ 基于路有装饰, 进行自定义扩展, Metadata(Rquired(:"admin")), 认证("/{id}" --> middleware（x, x, x） ,h.DescribeBuildConfig)
+ 基于路有装饰, 路有注册 --> 认证服务器 Resource("pods"), Action("list")
+ 基于路有装饰, 路有拦截 -- "/{id}" --> h.DescribeBuildConfig, 获取装饰信息，基于装饰信息来鉴权
