package main

import (
	"context"
	"fmt"

	"gitlab.com/go-course-project/go15/skills/grpc/interceptors"
	"gitlab.com/go-course-project/go15/skills/grpc/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {
	// Deprecated: use WithTransportCredentials and insecure.NewCredentials()
	conn, err := grpc.NewClient("localhost:1234",
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithPerRPCCredentials(interceptors.NewClientPerRPCCredentials("admin", "123456")),
	)
	if err != nil {
		panic(err)
	}

	client := pb.NewHelloServiceClient(conn)

	resp, err := client.Hello(context.TODO(), &pb.HelloRequest{Name: "bob"})
	if err != nil {
		panic(err)
	}
	fmt.Println(resp)

	// stream, err := client.Ping(context.TODO())
	// if err != nil {
	// 	panic(err)
	// }

	// // 在一个协程里收
	// go func() {
	// 	for {
	// 		resp, err := stream.Recv()
	// 		if err != nil {
	// 			fmt.Println(err)
	// 		} else {
	// 			fmt.Println(resp)
	// 		}
	// 	}
	// }()

	// // 在主流程里面发
	// for i := 0; i < 10; i++ {
	// 	if err := stream.Send(&pb.Message{Message: "ping"}); err != nil {
	// 		panic(err)
	// 	}
	// 	time.Sleep(1 * time.Second)
	// }
}
