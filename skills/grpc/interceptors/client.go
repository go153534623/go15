package interceptors

import "context"

func NewClientPerRPCCredentials(username, password string) *ClientPerRPCCredentials {
	return &ClientPerRPCCredentials{
		username: username,
		password: password,
	}
}

type ClientPerRPCCredentials struct {
	username string
	password string
}

// GetRequestMetadata gets the current request metadata, refreshing tokens
// if required. This should be called by the transport layer on each
// request, and the data should be populated in headers or other
// context. If a status code is returned, it will be used as the status for
// the RPC (restricted to an allowable set of codes as defined by gRFC
// A54). uri is the URI of the entry point for the request.  When supported
// by the underlying implementation, ctx can be used for timeout and
// cancellation. Additionally, RequestInfo data will be available via ctx
// to this call.  TODO(zhaoq): Define the set of the qualified keys instead
// of leaving it as an arbitrary string.
func (c *ClientPerRPCCredentials) GetRequestMetadata(
	ctx context.Context,
	uri ...string,
) (
	map[string]string,
	error,
) {

	return map[string]string{
		"username": c.username,
		"password": c.password,
	}, nil
}

// RequireTransportSecurity indicates whether the credentials requires
// transport security.
func (c *ClientPerRPCCredentials) RequireTransportSecurity() bool {
	return false
}
