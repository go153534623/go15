package mq_test

import (
	"context"
	"fmt"
	"log"
	"testing"

	"github.com/segmentio/kafka-go"
)

func TestListTopic(t *testing.T) {
	conn, err := kafka.Dial("tcp", "localhost:9092")
	if err != nil {
		panic(err.Error())
	}
	defer conn.Close()

	partitions, err := conn.ReadPartitions()
	if err != nil {
		panic(err.Error())
	}
	for _, p := range partitions {
		fmt.Println(p.Topic)
	}
}

func TestCreateTopic(t *testing.T) {
	conn, err := kafka.Dial("tcp", "localhost:9092")
	if err != nil {
		panic(err.Error())
	}
	defer conn.Close()

	err = conn.CreateTopics(kafka.TopicConfig{Topic: "maudit", NumPartitions: 3, ReplicationFactor: 1})
	if err != nil {
		t.Fatal(err)
	}
}

// 消费者
func TestConsumer(t *testing.T) {
	// make a new reader that consumes from topic-A
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{"localhost:9092"},
		// Consumer Groups, 不指定就是普通的一个Consumer
		GroupID: "cmdb",
		// 可以指定Partition消费消息
		// Partition: 0,

		Topic: "maudit",
		// MinBytes: 10e3, // 10KB
		// MaxBytes: 10e6, // 10MB
	})

	ctx := context.Background()
	for {
		// 自动确认
		// m, err := r.ReadMessage(context.Background())
		// if err != nil {
		// 	break
		// }
		// fmt.Printf("message at topic/partition/offset %v/%v/%v: %s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))

		// 手动确认
		m, err := r.FetchMessage(ctx)
		if err != nil {
			break
		}
		fmt.Printf("message at topic/partition/offset %v/%v/%v: %s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))

		// 处理完消息后需要提交该消息已经消费完成, 消费者挂掉后保存消息消费的状态
		if err := r.CommitMessages(ctx, m); err != nil {
			log.Fatal("failed to commit messages:", err)
		}
	}

	if err := r.Close(); err != nil {
		log.Fatal("failed to close reader:", err)
	}
}

// 生产者
func TestProducer(t *testing.T) {
	// make a writer that produces to maudit using the least-bytes distribution
	w := &kafka.Writer{
		Addr: kafka.TCP("localhost:9092"),
		// NOTE: When Topic is not defined here, each Message must define it instead.
		Topic: "maudit",
		// round-robin distribution
		Balancer: &kafka.Hash{},
		// The topic will be created if it is missing.
		// AllowAutoTopicCreation: true,
		// 支持消息压缩
		// Compression: kafka.Snappy,
		// 支持TLS
		// Transport: &kafka.Transport{
		//     TLS: &tls.Config{},
		// }
	}

	err := w.WriteMessages(context.Background(),
		kafka.Message{
			// 支持 Writing to multiple topics
			//  NOTE: Each Message has Topic defined, otherwise an error is returned.
			// Topic: "topic-A",
			Key:   []byte("Key-A"),
			Value: []byte("Hello World!"),
		},
		kafka.Message{
			Key:   []byte("KEY1"),
			Value: []byte("One!"),
		},
		kafka.Message{
			Key:   []byte("KEY1"),
			Value: []byte("Two!"),
		},
		kafka.Message{
			Key:   []byte("KEY1"),
			Value: []byte("Three!"),
		},
		kafka.Message{
			Key:   []byte("KEY1"),
			Value: []byte("Foure!"),
		},
	)

	if err != nil {
		log.Fatal("failed to write messages:", err)
	}

	if err := w.Close(); err != nil {
		log.Fatal("failed to close writer:", err)
	}
}
